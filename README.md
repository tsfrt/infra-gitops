
<img src="https://d1fto35gcfffzn.cloudfront.net/tanzu/tanzu-bug.svg" width="200" height="200" /> 
 
# Multi-Cloud GitOps with Tanzu

Apply Kapp controller to your managment cluster:

kapp deploy -a kc -f https://github.com/vmware-tanzu/carvel-kapp-controller/releases/latest/download/release.yml

kubectl create ns gitops

kubectl create serviceaccount gitops-sa -n gitops

kubectl create clusterrolebinding gitops-sa-crb --clusterrole=cluster-admin --serviceaccount=gitops:giptops-sa


then apply the TKG config:
kubectl apply -f tkg-config-library.yaml -n gitops

then apply the cluster "app"
kubectl apply -f aws-dev-devops-cluster.yaml -n gitops
